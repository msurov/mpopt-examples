import scipy.integrate as integrate
import numpy
from casadi import Function, SX
from scipy.integrate import ode

'''
def solveivp(statevar, rhsexpr, state0, tspan, step):
    t = SX.sym('t')
    f = Function('f', [t, statevar], [rhsexpr])
    r = ode(f)
    r.set_integrator('dopri5', atol=1e-8, rtol=1e-8, nsteps=20, max_step=1e-3)

    t1, t2 = tspan
    r.set_initial_value(state0, t1)
    n = int((t2 - t1) / step + 0.5)
    tarr = numpy.zeros(n)
    xarr = numpy.zeros((n,) + numpy.shape(state0))

    for i in range(n):
        t = t1 + i * (t2 - t1) / (n - 1)
        r.integrate(t)
        tarr[i] = r.t
        xarr[i] = r.y
    
    return tarr, xarr
'''

def solveivp(rhs, state0, tspan, step):
    r = ode(rhs)
    r.set_integrator('dopri5', atol=1e-8, rtol=1e-8, nsteps=20, max_step=1e-3)
    t1, t2 = tspan
    r.set_initial_value(state0, t1)
    n = int((t2 - t1) / step + 0.5)
    tarr = numpy.zeros(n)
    xarr = numpy.zeros((n,) + numpy.shape(state0))

    for i in range(n):
        t = t1 + i * (t2 - t1) / (n - 1)
        r.integrate(t)
        tarr[i] = r.t
        xarr[i] = r.y
    
    return tarr, xarr


def test():
    import matplotlib.pyplot as plt
    x = SX.sym('x', 2)
    f = numpy.array([x[1], -x[0]])
    tarr, xarr = solveivp(x, f, [0, 1], [0, 10], 1e-2)
    plt.plot(tarr, xarr)
    plt.grid()
    plt.show()


if __name__ == '__main__':
    test()
