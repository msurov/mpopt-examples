# Moon lander OCP direct collocation/multi-segment collocation
from mpopt import mp
import matplotlib.pyplot as plt


def rhs(x, u, t):
    return [x[1], u[0] - 1.5]

# Define OCP
ocp = mp.OCP(n_states=2, n_controls=1)
ocp.dynamics[0] = rhs
ocp.running_costs[0] = lambda x, u, t: u[0]**2
ocp.terminal_constraints[0] = lambda xf, tf, x0, t0: [xf[0] - 1.0, xf[1] - 1.0]
ocp.x00[0] = [10.0, -1.0]
ocp.lbu[0], ocp.ubu[0] = 0, 3

# Create optimizer(mpo), solve and post process(post) the solution
mpo, post = mp.solve(ocp, n_segments=20, poly_orders=3, scheme="LGR", plot=True)
x, u, t = post.get_data()

# plt.plot(t, x[:,0])
# plt.grid()
plt.show()
