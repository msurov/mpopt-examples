from casadi import SX, MX, DM, inv, Function, sin, cos, vertcat, pi, \
    integrator, nlpsol, substitute, rootfinder, simplify
import numpy as np
from mpopt import mp
import matplotlib.pyplot as plt
from simulate import solveivp
from animate import anim2
from dynamics import Dynamics


################################## 
# symbolic dynamics

t = SX.sym('t')
nlinks = 2
ndof = nlinks + 1
dynamics = Dynamics(nlinks)
dynamics_fun = Function('dynamics', [dynamics.x, dynamics.u, t], [dynamics.rhs])
energy_fun = Function('energy', [dynamics.x], [dynamics.E])


################################## 
# boundary

q0 = np.zeros(ndof)
q0[1:] = -np.pi/2
dq0 = np.zeros(ndof)
x0 = np.concatenate([q0, dq0])

qf = np.zeros(ndof)
qf[1] = 1.
qf[1:] = np.pi/2
dqf = np.zeros(ndof)
xf = np.concatenate([qf, dqf])


################################## 
# cost

cost_expr = dynamics.u.T @ dynamics.u
cost_fun = Function('cost', [dynamics.x, dynamics.u, t], [cost_expr])

################################## 
# optimization

ocp = mp.OCP(n_states=2*ndof, n_controls=1)
ocp.dynamics[0] = lambda x, u, t: dynamics_fun(x, u, t).elements()
ocp.running_costs[0] = lambda x, u, t: cost_fun(x, u, t)
ocp.terminal_constraints[0] = lambda _xf, _tf, _x0, _t0: SX(_xf - xf).elements()
ocp.x00[0] = x0
ocp.lbu[0] = -10
ocp.ubu[0] = 10

mpo, post = mp.solve(ocp, n_segments=100, poly_orders=3, scheme="LGR", plot=True)
x, u, t = post.get_data()
plt.show()
# anim2(t, x[:,0:ndof], filename='out-2.mp4')

