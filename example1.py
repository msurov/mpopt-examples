from casadi import SX, MX, DM, inv, Function, sin, cos, vertcat, pi
import numpy as np
from mpopt import mp
import matplotlib.pyplot as plt
from animate import anim2
from scipy.interpolate import make_interp_spline
from simulate import solveivp


################################## 
# symbolic dynamics

t = SX.sym('t')
q = SX.sym('q', 3)
dq = SX.sym('dq', 3)
x = vertcat(q, dq)
u = SX.sym('u', 1)
M = np.array([
    [3, -2*sin(q[1]), -sin(q[2])],
    [-2*sin(q[1]), 2, cos(q[1] - q[2])],
    [-sin(q[2]), cos(q[1] - q[2]), 1]
])
C = np.array([
    [0, -2*dq[1]*cos(q[1]), -dq[2] * cos(q[2])],
    [0, 0, dq[2]*sin(q[1]-q[2])],
    [0, -dq[1]*sin(q[1]-q[2]), 0]
])
G = np.array([
    0,
    2*cos(q[1]),
    cos(q[2])
])
B = np.array([
    [1],
    [0],
    [0]
])
dynamics = vertcat(dq, inv(M) @ (-C @ dq - G + B @ u))
dynamics_fun = Function('dynamics', [x, u, t], [dynamics])

################################## 
# boundary

q0 = np.array([1., -0.1, -0.2])
dq0 = np.array([0., 0., 0.])
x0 = np.concatenate([q0, dq0])

qf = np.array([0., 0.1, -0.05])
dqf = np.array([0., 0., 0.])
xf = np.concatenate([qf, dqf])

################################## 
# cost

cost_expr = u.T @ u
cost_fun = Function('cost', [x, u, t], [cost_expr])

################################## 
# optimization problem

ocp = mp.OCP(n_states=6, n_controls=1)
ocp.dynamics[0] = lambda x, u, t: dynamics_fun(x, u, t).elements()
ocp.running_costs[0] = lambda x, u, t: cost_fun(x, u, t)
ocp.terminal_constraints[0] = lambda _xf, _tf, _x0, _t0: SX(_xf - xf).elements()
ocp.x00[0] = x0
ocp.lbu[0] = -10
ocp.ubu[0] = 10

################################## 
# solve

mpo, post = mp.solve(ocp, n_segments=55, poly_orders=3, scheme="LGR", plot=False)
x, u, t = post.get_data()

_,axes = plt.subplots(2, 1, sharex=True)
plt.sca(axes[0])
plt.plot(t, x[:,0:3])
plt.legend([R'$x$', R'$\theta_1$', R'$\theta_2$'])
plt.grid()

plt.sca(axes[1])
plt.plot(t, x[:,3:6])
plt.legend([R'$\dot x$', R'$\dot \theta_1$', R'$\dot \theta_2$'])
plt.grid()
plt.show()
