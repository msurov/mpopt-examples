from casadi import SX, MX, DM, inv, Function, sin, cos, vertcat, pi, \
    integrator, nlpsol, substitute, rootfinder, gradient, jacobian, \
    reshape, sum1, diag, simplify
import numpy as np


def expr_M(q):
    ndof = q.size1()
    M = np.zeros((ndof, ndof), dtype=SX)
    sinq = sin(q)

    for i in range(0, ndof):
        M[i,i] = ndof - i

        for j in range(i + 1, ndof):
            if i == 0:
                M[i,j] = -(ndof - j) * sinq[j]
            else:
                M[i,j] = (ndof - j) * cos(q[i] - q[j])
            M[j,i] = M[i,j]

    return M


def expr_C_aut(q, dq):
    # auto
    M = expr_M(q)
    Mdq = M @ dq
    D = jacobian(Mdq, q)
    C = D - D.T / 2
    return C


def expr_C(q, dq):
    # manual
    ndof = q.size1()
    C = np.zeros((ndof, ndof), dtype=SX)
    for i in range(1, ndof):
        for j in range(i + 1, ndof):
            elem = (ndof - j) * sin(q[i] - q[j])
            C[i,j] = elem
            C[j,i] = -elem

    for j in range(1, ndof):
        C[0, j] = -(ndof - j) * cos(q[j])

    return C @ diag(dq)


def expr_U(q):
    ndof = q.size1()
    p = SX([ndof - i for i in range(1, ndof)])
    return p.T @ sin(q[1:])


def expr_G_aut(q):
    U = expr_U(q)
    return gradient(U, q)


def expr_G(q):
    ndof = q.size1()
    G = np.zeros(ndof, dtype=SX)
    for i in range(1, ndof):
        G[i] = (ndof - i) * cos(q[i])
    return G

def expr_K(q, dq):
    M = expr_M(q)
    return dq.T @ M @ dq / 2


def expr_E(q, dq):
    return expr_K(q, dq) + expr_U(q)


def expr_B(q):
    ndof = q.size1()
    B = np.zeros(ndof, dtype=SX)
    B[0] = 1
    return B


class Dynamics:
    def __init__(self, nlinks):
        n = nlinks + 1
        q = SX.sym('q', n)
        dq = SX.sym('dq', n)
        u = SX.sym('u', 1)

        M = expr_M(q)
        C = expr_C(q, dq)
        G = expr_G(q)
        B = expr_B(q)
        E = expr_E(q, dq)

        self.nlinks = nlinks
        self.ndof = n

        self.q = q
        self.dq = dq
        self.x = vertcat(q, dq)
        self.u = u
        self.M = M
        self.C = C
        self.G = G
        self.B = B
        self.E = E

        ddq = inv(M) @ (-C @ dq - G + B @ u)
        rhs = vertcat(dq, ddq)
        self.rhs = rhs

    def rhs_numeric(self):
        f = Function('Cart%dLinksPendDynamics' % self.nlinks,
            [self.x, self.u], [self.rhs])
        return f

    def energy_numeric(self):
        f = Function('Cart%dLinksPendEnergy' % self.nlinks,
            [self.x], [self.E])
        return f


def test_G():
    nlinks = 5
    n = nlinks + 1
    q = SX.sym('q', n)
    G1 = expr_G(q)
    G2 = expr_G_aut(q)
    z = G1 - G2
    zf = Function('dummy', [q], [z])
    assert np.allclose(zf(np.random.normal(size=n)), 0.)


def test_simulate():
    from simulate import solveivp
    from animate import anim2
    import matplotlib.pyplot as plt

    dynamics = Dynamics(10)
    f = dynamics.rhs_numeric()
    rhs = lambda _, x: f(x, 0)
    state0 = np.zeros(dynamics.x.size1())
    t, x = solveivp(rhs, state0, [0, 10], 1e-2)

    E = dynamics.energy_numeric()
    E_vals = [E(xi) for xi in x]

    anim2(t, x[:,0:dynamics.ndof], filename='out-1.mp4')
    plt.show()


if __name__ == '__main__':
    test_G()
