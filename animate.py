import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from bisect import bisect_left


def interpolate(t, t_arr, x_arr, y_arr):
    i = bisect_left(t_arr, t) - 1
    if i == len(t_arr) - 1:
        x = x_arr[i]
        y = np.concatenate(([0], y_arr[i]))
        return x, y

    t1 = t_arr[i]
    t2 = t_arr[i+1]
    c1 = (t - t1) / (t2 - t1)
    c2 = (t2 - t) / (t2 - t1)

    x = x_arr[i] * c2 + x_arr[i+1] * c1
    _y = y_arr[i] * c2 + y_arr[i+1] * c1
    y = np.concatenate(([0], _y))

    return x, y


def anim(t_arr, x_arr, y_arr, speed=1., fps=60., filename=None):
    fig = plt.gcf()
    fig.set_size_inches(6*3, 3*3)

    ax = plt.subplot2grid((1, 1), (0, 0), rowspan=1, colspan=1, aspect=1)
    plt.grid()
    ax.set_xlim(np.min(x_arr), np.max(x_arr))
    ax.set_ylim(min(0, np.min(y_arr)), max(0, np.max(y_arr)))
    ax.set_title('play speed %d%%' % int(speed * 100))
    ax.set_aspect('equal')
    fig.tight_layout()

    text_pos = [0.1, 0.1]
    text = ax.text(text_pos[0], text_pos[1], 'xx', bbox=dict(facecolor='gray', alpha=0.2), transform = ax.transAxes)

    x = x_arr[0]
    y = np.concatenate(([0], y_arr[0]))
    polygon, = plt.plot(x, y, '-o')

    plt.plot(x, y, '-o', color='gray')

    def upt(f):
        t = speed * f * interval / 1000.
        text.set_text('t = %2.2fs' % t)
        x,y = interpolate(t, t_arr, x_arr, y_arr)
        polygon.set_xdata(x)
        polygon.set_ydata(y)
        return polygon

    interval = 1000. / fps
    nframes = int((t_arr[-1] / speed) * 1000. / interval)
    a = animation.FuncAnimation(fig, upt, frames=nframes, interval=interval, blit=False)
    if filename is None:
        plt.show()
    else:
        bitrate = 200. * fps
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=fps, metadata=dict(artist='Me'), bitrate=bitrate)
        print('recording movie..')
        a.save(filename, writer=writer)
        print(' done')


def generalized_to_cartesean(q):
    R'''
        [x0,theta] -> x,y
        assume all lengths equal 1
    '''
    sq = np.shape(q)
    if len(sq) == 1:
        _q = np.reshape(q, (1,-1))
        x,y = generalized_to_cartesean(_q)
        return x[0], y[0]

    k = sq[0]
    n = sq[1]

    x0 = q[:,0]
    theta = q[:,1:]

    x = np.zeros((k, n))
    x[:,0] = x0
    x[:,1:] = np.cos(theta)
    x = np.cumsum(x, axis=1)

    y = np.zeros((k, n-1))
    y = np.sin(theta)
    y = np.cumsum(y, axis=1)

    return x, y


def anim2(t_arr, q_arr, **kwargs):
    x_arr, y_arr = generalized_to_cartesean(q_arr)
    return anim(t_arr, x_arr, y_arr, **kwargs)
